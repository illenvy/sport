from rest_framework.viewsets import ModelViewSet
from team.serializers import TeamSerializer
from team.models import Team

class TeamViewSet(ModelViewSet):
    queryset = Team.objects.all()
    serializer_class = TeamSerializer