from django.db import models
from couch.models import Couch
from type.models import Type
from player.models import Player
from country.models import Country

class Team(models.Model):
    couch = models.OneToOneField(Couch, on_delete = models.CASCADE)
    title = models.CharField(verbose_name='Название команды', max_length=255)
    description = models.TextField(verbose_name='О команде')
    type = models.ForeignKey(Type, on_delete = models.CASCADE)
    country = models.ForeignKey(Country, on_delete = models.CASCADE)
    player = models.ManyToManyField(verbose_name='Игроки', to=Player, related_name='teams')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Команда'
        verbose_name_plural = 'Команды'