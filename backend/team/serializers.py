from rest_framework import serializers
from team.models import Team
from player.models import Player

class PlayerSerializerForTeam(serializers.ModelSerializer):
    class Meta:
        model = Player
        fields = ['first_name', 'last_name', 'country', 'type']

class TeamSerializer(serializers.ModelSerializer):
    players_data = PlayerSerializerForTeam(source='player', many=True)
    class Meta:
        model = Team
        exclude = ['player']