from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin

from authentication.managers import UserManager

# id django дописывает сам

class User(AbstractBaseUser, PermissionsMixin): #AbstractBaseUser сам дописывает пароль
    # verbose_name = текст для админ. панели
    email = models.EmailField(verbose_name='Email адрес', max_length=255, unique=True)
    first_name = models.CharField(verbose_name='Имя', max_length=255)
    last_name = models.CharField(verbose_name='Фамилия', max_length=255)
    bio = models.TextField(verbose_name='О себе') #у bio тип text, так что max_length не нужен

    # Активированный пользователь
    is_active = models.BooleanField(verbose_name='Активирован', default=False)
    # Доступ к админ панели
    is_staff = models.BooleanField(verbose_name='Персонал', default=False)
    # Права администратора
    is_superuser = models.BooleanField(verbose_name='Администратор', default=False)

    USERNAME_FIELD = 'email'
    #Какие поля обязан иметь пользователь
    REQUIRED_FIELDS = ['first_name', 'last_name', ]

    objects = UserManager()

    #Функция, которая отвечает за то, как наша модель пользователя будет выводиться в админ. панели
    def __str__(self):
        return self.email

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'