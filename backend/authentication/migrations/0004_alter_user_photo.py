# Generated by Django 3.2.13 on 2022-07-07 07:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0003_auto_20220626_0314'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='photo',
            field=models.FileField(upload_to='users/photos', verbose_name='Фото'),
        ),
    ]
