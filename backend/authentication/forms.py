from .models import User
from django.forms import ModelForm, TextInput, Textarea

class UserForm(ModelForm):
    class Meta:
        model = User
        fields = ["email", "first_name", "last_name", "bio", "password"]
        widgets = {
            "email": TextInput(attrs={
                'class': 'form__input',
                'type': 'email',
                'name': 'email',
                'id': 'email',
                'placeholder': 'Введите E-mail'
            }),
            "first_name": TextInput(attrs={
                'class': 'form__input',
                'type': 'text',
                'name': 'first_name',
                'id': 'first_name',
                'placeholder': 'Введите имя'
            }),
            "last_name": TextInput(attrs={
                'class': 'form__input',
                'type': 'text',
                'name': 'last_name',
                'id': 'last_name',
                'placeholder': 'Введите фамилию'
            }),
            "bio": Textarea(attrs={
                'class': 'form__input',
                'name': 'bio',
                'id': 'bio',
                'placeholder': 'Расскажите о себе',
                'style': 'resize:none; width: 250px; height: 150px'
            }),
            "password": TextInput(attrs={
                'class': 'form__input',
                'type': 'password',
                'name': 'password',
                'id': 'password',
                'placeholder': 'Введите пароль'
            }),
            }