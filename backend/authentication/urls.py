from urllib.parse import urlparse
from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.index, name='home'),
    path('login', views.login, name='login'),
    path('register', views.register, name='register'),
    path('authorized', views.authorized, name='authorized'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
