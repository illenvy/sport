from urllib import response
from rest_framework.exceptions import ValidationError, NotFound, AuthenticationFailed
from rest_framework.viewsets import ModelViewSet
from authentication.models import User
from authentication.serializers import UserSerializer
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.permissions import IsAuthenticated
from django.shortcuts import render, redirect

from player.models import Player
from .models import User
from .forms import UserForm

class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    @action(methods=['POST'], detail=False, url_path='register')
    def register(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({'message': 'success'})

    @action(methods=['POST'], detail=False, url_path='login')
    def login(self, request):
        if 'email' not in request.data:
            raise ValidationError({'error': 'email must not be empty'})
        if 'password' not in request.data:
            raise ValidationError({'error': 'password must not be empty'})

        try:
            user = User.objects.get(email=request.data['email'])
        except User.DoesNotExist:
            raise NotFound({'error': 'user with this email was not found'})

        if not user.check_password(request.data['password']):
            raise AuthenticationFailed({'error': 'incorrect password'})

        if not user.is_active:
            raise AuthenticationFailed({'error': 'user is not activated'})

        refresh = RefreshToken.for_user(user)
        response = Response()
        response.set_cookie('refresh', str(refresh))
        response.data = {'access': str(refresh.access_token)}
        return response

    @action(methods=['GET'], detail=False, permission_classes=[IsAuthenticated], url_path='me')
    def get_user(self, request):
        user = request.user
        data = self.serializer_class(user).data
        return Response(data)

    @action(methods=['POST'], detail=False, url_path='logout', permission_classes=[IsAuthenticated])
    def logout(self, request):
            response = Response()
            response.delete_cookie('refresh')
            return response



def index(request):
    title = 'Главная страница сайта'
    context = {
        'title': title
    }
    return render(request, 'authentication/index.html', context)

def authorized(request):
    title = 'Успешно'
    context = {
        'title': title
    }
    return render(request, 'authentication/authorized.html', context)

def login(request):
    title = 'Логин'
    error = ''
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
        else:
            error = 'Вы неправильно заполнили форму'
            
    form = UserForm()
    context = {
        'form': form,
        'error': error,
        'title': title
    }
    return render(request, 'authentication/login.html', context)

def register(request):
    title = 'Регистрация'
    error = ''
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')
        else:
            error = 'Вы неправильно заполнили форму'

    form = UserForm()
    context = {
        'form': form,
        'error': error,
        'title': title
    }
    return render(request, 'authentication/register.html', context)
