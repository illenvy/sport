from django.db import models

class Result(models.Model):
    title = models.CharField(verbose_name='Результат', max_length=255)
    description = models.TextField(verbose_name='О результате')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Результат'
        verbose_name_plural = 'Результаты'