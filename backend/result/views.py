from rest_framework.viewsets import ModelViewSet
from result.serializers import ResultSerializer
from result.models import Result
from django.shortcuts import render, redirect
from django.views.generic import DetailView, UpdateView, DeleteView

class ResultViewSet(ModelViewSet):
    queryset = Result.objects.all()
    serializer_class = ResultSerializer

def results(request):
    title = 'Результат'
    results = Result.objects.all()
    context = {
        'title': title,
        'results': results
    }
    return render(request, 'result/results.html', context)