from requests import Response
from rest_framework.viewsets import ModelViewSet
from player.forms import PlayerForm
from player.serializers import PlayerSerializer, FavoritePlayerSerializer
from player.models import Player, FavoritePlayer
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.exceptions import NotFound
from django.views.generic import DetailView
from django.shortcuts import render, redirect
from django.views.generic import DetailView, UpdateView, DeleteView
from rest_framework.pagination import PageNumberPagination


# Пагинация
class PlayerPagination(PageNumberPagination):
    page_size = 2
    page_query_param = 'page_size'
    max_page_size = 10000

class PlayerViewSet(ModelViewSet):
    queryset = Player.objects.all()
    serializer_class = PlayerSerializer
    pagination_class = PlayerPagination

    @action(methods=['POST'], detail=True, permission_classes=[IsAuthenticated], url_path='toggle-favorite')
    def toggle_favorite(self, request, pk=None):
        user = request.user
        try:
            player = self.queryset.get(pk=pk)
        except Player.DoesNotExist:
            raise NotFound('player is not found')
        try:
            fav_player = FavoritePlayer.objects.get(user=user, player=player)
            fav_player.delete()
            return Response({'message': 'removed'})
        except FavoritePlayer.DoesNotExist:
            FavoritePlayer.objects.create(user=user, player=player)
            return Response({'message': 'added'})

    @action(methods=['GET'], detail=False, permission_classes=[IsAuthenticated], url_path='favorites')
    def get_favorites(self, request):
        user = request.user
        players = FavoritePlayer.objects.filter(user=user)
        data = FavoritePlayerSerializer(instance=players, many=True).data
        return Response(data)

class PlayerDetailView(DetailView):
    model = Player
    template_name = 'player/details_view.html'
    context_object_name = 'player'

class PlayerUpdateView(UpdateView):
    model = Player
    template_name = 'player/update.html'

    form_class = PlayerForm

class PlayerDeleteView(DeleteView):
    model = Player
    success_url = '/players/'
    template_name = 'player/player-delete.html'

def players(request):
    title = 'Игроки'
    players = Player.objects.all()
    context = {
        'title': title,
        'players': players
    }
    return render(request, 'player/players.html', context)

def create(request):
    error = ''
    if request.method == 'POST':
        form = PlayerForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('../')
        else:
            error = 'Вы неправильно ввели форму'

    form = PlayerForm()

    data = {
        'form': form,
        'error': error
    }

    return render(request, 'player/create.html', data)