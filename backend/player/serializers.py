from rest_framework import serializers
from player.models import FavoritePlayer, Player
from authentication.serializers import User, UserSerializer

class PlayerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Player
        fields = '__all__'

class FavoritePlayerSerializer(serializers.ModelSerializer):
    user_data = UserSerializer(source='user')
    player_data = PlayerSerializer(source='player')
    class Meta:
        model = FavoritePlayer
        exclude = ['user', 'player']