from urllib.parse import urlparse
from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.players, name='players'),
    path('create/', views.create, name='create'),
    path('<int:pk>', views.PlayerDetailView.as_view(), name='players-detail'),
    path('<int:pk>/update', views.PlayerUpdateView.as_view(), name='players-update'),
    path('<int:pk>/delete', views.PlayerDeleteView.as_view(), name='players-delete'),
]
