from tabnanny import verbose
from django.db import models
from authentication.models import User

class Player(models.Model):
    first_name = models.CharField(verbose_name='Имя', max_length=255)
    last_name = models.CharField(verbose_name='Фамилия', max_length=255)
    bio = models.TextField(verbose_name='О себе')
    date_of_birth = models.DateField(verbose_name='Дата рождения')

    def __str__(self):
        return self.last_name

    def get_absolute_url(self):
        return f'/players/{self.id}'
    

    class Meta:
        verbose_name = 'Игрок'
        verbose_name_plural = 'Игроки'

class FavoritePlayer(models.Model):
    user = models.ForeignKey(User, verbose_name='User', on_delete=models.CASCADE)
    player = models.ForeignKey(Player, verbose_name='User', on_delete=models.CASCADE)