from rest_framework.viewsets import ModelViewSet
from couch.serializers import CouchSerializer
from couch.models import Couch
from couch.forms import CouchForm
from django.shortcuts import render, redirect
from django.views.generic import DetailView, UpdateView, DeleteView
from django.db.models import Q
from rest_framework.pagination import PageNumberPagination
import django_filters.rest_framework
from rest_framework import filters


# Пагинация
class CouchPagination(PageNumberPagination):
    page_size = 2
    page_query_param = 'page_size'
    max_page_size = 10000

class CouchViewSet(ModelViewSet):
    queryset = Couch.objects.all()
    serializer_class = CouchSerializer
    pagination_class = CouchPagination

    # django_filters.rest_framework.DjangoFilterBackend
    # filter_backends = [django_filters.rest_framework.DjangoFilterBackend]

    # SearchFilter
    filter_backends = [filters.SearchFilter]
    search_fields = ['first_name', 'last_name']

def couches(request):
    title = 'Тренеры'
    couches = Couch.objects.all()
    context = {
        'title': title,
        'couches': couches,
    }
    return render(request, 'couch/couches.html', context)

def couchesFilter(request):
    title = 'Отфильтрованные тренеры'
    couches = Couch.objects.filter(
        Q(first_name__startswith='И')&
        Q(last_name__startswith='И')
        )
    context = {
        'title': title,
        'couches': couches
    }
    return render(request, 'couch/filter.html', context)

def create(request):
    error = ''
    if request.method == 'POST':
        form = CouchForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('../')
        else:
            error = 'Вы неправильно ввели форму'

    form = CouchForm()

    data = {
        'form': form,
        'error': error
    }

    return render(request, 'couch/create.html', data)

class CouchDetailView(DetailView):
    model = Couch
    template_name = 'couch/details_view.html'
    context_object_name = 'couch'

class CouchUpdateView(UpdateView):
    model = Couch
    template_name = 'couch/update.html'

    form_class = CouchForm

class CouchDeleteView(DeleteView):
    model = Couch
    success_url = '/couches/'
    template_name = 'couch/couch-delete.html'