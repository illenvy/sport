from django.urls import path
from . import views

urlpatterns = [
    path('', views.couches, name='couches'),
    path('filter', views.couchesFilter, name='couches-filter'),
    path('create/', views.create, name='create'),
    path('<int:pk>', views.CouchDetailView.as_view(), name='couches-detail'),
    path('<int:pk>/update', views.CouchUpdateView.as_view(), name='couches-update'),
    path('<int:pk>/delete', views.CouchDeleteView.as_view(), name='couches-delete'),
]
