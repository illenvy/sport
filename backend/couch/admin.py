from django.contrib import admin
from couch.models import Couch
from import_export.admin import ImportExportModelAdmin
from simple_history.admin import SimpleHistoryAdmin


@admin.register(Couch)
class MemberAdmin(ImportExportModelAdmin, SimpleHistoryAdmin):
    list_display = ("first_name", "last_name", "bio", "date_of_birth")
    pass