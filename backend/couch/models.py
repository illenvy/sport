from django.db import models
from simple_history.models import HistoricalRecords
import django_filters

class Couch(models.Model):
    first_name = models.CharField(verbose_name='Имя', max_length=255)
    last_name = models.CharField(verbose_name='Фамилия', max_length=255)
    bio = models.TextField(verbose_name='О себе')
    date_of_birth = models.DateField(verbose_name='Дата рождения')
    # история изменения объекта
    history = HistoricalRecords()

    def __str__(self):
        return self.last_name

    def get_absolute_url(self):
        return f'/couches/{self.id}'

    class Meta:
        verbose_name = 'Тренер'
        verbose_name_plural = 'Тренеры'
        db_table = "couch_couch"

# django_filters
class CouchFilter(django_filters.FilterSet):
    class Meta:
        model = Couch
        fields = ['first_name', 'last_name']