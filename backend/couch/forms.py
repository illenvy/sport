from .models import Couch
from django.forms import DateField, ModelForm, TextInput, Textarea
from django.forms.widgets import DateInput

class CouchForm(ModelForm):
    class Meta:
        model = Couch
        fields = ["first_name", "last_name", "bio", "date_of_birth"]
        labels = {
            'date_of_birth': ('D.O.B')
        }
        widgets = {
            "first_name": TextInput(attrs={
                'class': 'form__input',
                'type': 'text',
                'name': 'first_name',
                'id': 'first_name',
                'placeholder': 'Введите имя'
            }),
            "last_name": TextInput(attrs={
                'class': 'form__input',
                'type': 'text',
                'name': 'last_name',
                'id': 'last_name',
                'placeholder': 'Введите фамилию'
            }),
            "bio": Textarea(attrs={
                'class': 'form__input',
                'name': 'bio',
                'id': 'bio',
                'placeholder': 'Расскажите о себе',
                'style': 'resize:none; width: 250px; height: 150px'
            }),
            "date_of_birth": DateInput(attrs={
                'type': 'date',
                'id': 'dob',
                'class': 'form__input'
            })
            }