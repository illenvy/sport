from rest_framework.viewsets import ModelViewSet
from type.serializers import TypeSerializer
from type.models import Type

class TypeViewSet(ModelViewSet):
    queryset = Type.objects.all()
    serializer_class = TypeSerializer