from django.db import models

class Type(models.Model):
    sport = models.CharField(verbose_name='Тип спорта', max_length=255)
    description = models.TextField(verbose_name='О спорте')

    def __str__(self):
        return self.sport

    class Meta:
        verbose_name = 'Тип спорта'
        verbose_name_plural = 'Типы спорта'