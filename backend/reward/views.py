from rest_framework.viewsets import ModelViewSet
from reward.serializers import RewardSerializer
from reward.models import Reward
from django.shortcuts import render, redirect
from django.views.generic import DetailView, UpdateView, DeleteView

class RewardViewSet(ModelViewSet):
    queryset = Reward.objects.all()
    serializer_class = RewardSerializer

def rewards(request):
    title = 'Награда'
    rewards = Reward.objects.all()
    context = {
        'title': title,
        'rewards': rewards
    }
    return render(request, 'reward/rewards.html', context)