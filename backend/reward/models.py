from django.db import models

class Reward(models.Model):
    title = models.CharField(verbose_name='Награда', max_length=255)
    description = models.TextField(verbose_name='О награде')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Награда'
        verbose_name_plural = 'Награды'