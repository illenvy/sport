from rest_framework.routers import DefaultRouter
from couch.views import CouchViewSet
from country.views import CountryViewSet
from event.views import EventViewSet
from player.views import PlayerViewSet
from result.views import ResultViewSet
from reward.views import RewardViewSet
from team.views import TeamViewSet
from type.views import TypeViewSet
from authentication.views import UserViewSet

router = DefaultRouter()

router.register('couch', CouchViewSet)
router.register('country', CountryViewSet)
router.register('event', EventViewSet)
router.register('player', PlayerViewSet)
router.register('result', ResultViewSet)
router.register('reward', RewardViewSet)
router.register('team', TeamViewSet)
router.register('type', TypeViewSet)
router.register('auth', UserViewSet)