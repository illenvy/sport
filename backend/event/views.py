from rest_framework.viewsets import ModelViewSet
from event.serializers import EventSerializer
from event.models import Event
from django.shortcuts import render, redirect
from django.views.generic import DetailView, UpdateView, DeleteView

class EventViewSet(ModelViewSet):
    queryset = Event.objects.all()
    serializer_class = EventSerializer

def events(request):
    title = 'Мероприятия и новости'
    events = Event.objects.all()
    context = {
        'title': title,
        'events': events
    }
    return render(request, 'event/events.html', context)

class EventDetailView(DetailView):
    model = Event
    template_name = 'event/details_view.html'
    context_object_name = 'event'