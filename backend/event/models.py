from django.db import models
from team.models import Team
from result.models import Result
from reward.models import Reward

class Event(models.Model):
    title = models.CharField(verbose_name='Название мероприятия', max_length=255)
    description = models.TextField(verbose_name='О мероприятии')
    team = models.ManyToManyField(verbose_name='Команды', to=Team, related_name='events')
    result = models.ForeignKey(Result, on_delete = models.CASCADE)
    reward = models.ForeignKey(Reward, on_delete = models.CASCADE)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Мероприятие'
        verbose_name_plural = 'Мероприятия'