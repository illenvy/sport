from urllib.parse import urlparse
from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.events, name='events'),
    path('<int:pk>', views.EventDetailView.as_view(), name='events-detail'),
]
