from rest_framework import serializers
from event.models import Event
from team.models import Team

class TeamSerializerForEvent(serializers.ModelSerializer):
    class Meta:
        model = Team
        fields = ['title', 'type', 'country', 'player']

class EventSerializer(serializers.ModelSerializer):
    teams_data = TeamSerializerForEvent(source='team', many=True)
    class Meta:
        model = Event
        exclude = ['team']