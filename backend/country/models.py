from django.db import models

class Country(models.Model):
    country = models.CharField(verbose_name='Страна', max_length=255)
    description = models.TextField(verbose_name='О стране')

    def __str__(self):
        return self.country

    class Meta:
        verbose_name = 'Страна'
        verbose_name_plural = 'Страны'