from rest_framework.viewsets import ModelViewSet
from country.serializers import CountrySerializer
from country.models import Country

class CountryViewSet(ModelViewSet):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer